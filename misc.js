module.exports = {

  getTimestamp: function() {
    var d = new Date();
    return d.getTime() / 1000 | 0;
  }

}
