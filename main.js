const fs = require( 'fs' );
const yaml = require( 'js-yaml' );
const express = require( 'express' );
const http = require( 'http' );

const misc = require( './misc' );


const log = console.log.bind( console );


// config
try {
  const config = yaml.safeLoad( fs.readFileSync( './config.yml', 'utf8' ) );
  // log( config );
}
catch( e ) {
  log( e );
  process.exit( 1 );
}


// servingExample
if( config.servingExample.enabled ) {
  const servingExample = require( './servingExample' );
  servingExample.server( config );
}


// main app
function atExec( at ) {
  var req = http.request(
    {
      method: 'GET',
      host: at.host,
      path: at.path,
      port: at.port
    },
    function( response ) {
      var str = '';
      response
        .on( 'data', function( chunk ) {
          str += chunk;
        } )
        .on( 'end', function() {
        } );
    }
  ).end();
}

function addAt( at ) {
  var time = at.time - misc.getTimestamp();
  time = time > 0 ? time : 0;
  at.timeout = setTimeout( function() {
    // log( at );
    log(
      (new Date()).toISOString() + ' - WAt Server timeout',
      at.host + ':' + at.port + at.path
    );
    atExec( at );
  }, time * 1000 );
  atList.push( at );
}

function loadAtList() {
  for( i in config.atList ) {
    var req = http.request(
      {
        method: 'GET',
        host: config.atList[ i ].host,
        path: config.atList[ i ].path,
        port: config.atList[ i ].port
      },
      function( response ) {
        var str = '';
        response
          .on( 'data', function( chunk ) {
            str += chunk;
          } )
          .on( 'end', function() {
            var at = JSON.parse( str );
            // log( at );
            for( j in at ) {
              addAt( at[ j ] );
            }
          } );
      }
    ).end();
  }
}


const app = express();

app.use( function( req, res, next ) {
  log( (new Date()).toISOString() + ' - WAt request ' + req.path );
  next();
} );

app.get( '/', function( req, res ) {
  res.set( 'Content-Type', 'text/plain' );
  res.send( 'WAt' );
} );

const server = app.listen( config.port, function() {
  log(
    (new Date()).toISOString() +
    ' - WAt Server running at http://127.0.0.1:' +
    config.port + '/'
  );
} );


var atList = [];
loadAtList();
