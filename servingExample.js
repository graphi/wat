const express = require( 'express' );

const misc = require( './misc' );


const log = console.log.bind( console );


module.exports = {

  server: function ( config ) {
    const app = express();

    app.use( function( req, res, next ) {
      log( (new Date()).toISOString() + ' - Example Server request ' + req.path );
      next();
    } );

    app.get( '/', function( req, res ) {
      res.set( 'Content-Type', 'text/plain' );
      res.send( 'servingExample' );
    } );

    app.get( '/list', function( req, res ) {
      res.set( 'Content-Type', 'text/plain' );
      res.send( JSON.stringify( [
        {
          host: 'localhost',
          port: config.servingExample.port,
          path: '/a',
          time: misc.getTimestamp() + 3,
        },
        {
          host: 'localhost',
          port: config.servingExample.port,
          path: '/b',
          time: misc.getTimestamp() + 5,
        },
      ], null, 4 ) );
    } );

    const server = app.listen( config.servingExample.port, function () {
      log(
        (new Date()).toISOString() +
        ' - Example Server running at http://127.0.0.1:' +
        config.servingExample.port + '/'
      );
    } );
  },

};
